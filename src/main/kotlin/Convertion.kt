/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {

    val metters=km*1000

        return metters.toInt()
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        val metters=km*1000

        return metters
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
        val cm=km*100000

        return cm
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        val metters = mm*0.001
        return metters
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
            val foot = miles*5280
        return foot
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {

        val inches = yard*(36/10)
        return inches
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return (inch * (1.0 / 39.37) * (1.0 / 1000.0) * (0.621371 / 1.0))
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        val yards= foot*0.333333

        return yards.toInt()
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {
        val kmtoinches = km?.toDoubleOrNull()
        return (kmtoinches!! * (1000 / 1) * (100 / 1) * (1 / 2.54))
    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        val mmtofoots = mm?.toDouble()
        return (mmtofoots!! * (1 / 304.8))
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        val yardtocm = yard?.toDouble()
        return (yardtocm!! * (0.9144 / 1) * (100 / 1))
    }
}