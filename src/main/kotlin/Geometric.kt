
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * @author sigmotoa
 */

class Geometric {
    //Calculate the area for a square using one side
    fun squareArea(side: Int): Int {
            val area = side * side
        return area;
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {
        val area = sideA*sideB
        return area
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {

       val area=(radius*radius)*Math.PI
        return area
    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {


        val perimeter = Math.PI*diameter

        return perimeter
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {

        val perimeter = 4.0*(side)

        return perimeter
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        val vSphere=(4.0/3.0)*Math.PI*radius.pow(3)
        return vSphere
    }

    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        val resultado = ((sqrt(5.00 * (5.00 + 2.00 * (sqrt(5.0).toFloat()))).toFloat() * side.toFloat() * side.toFloat()) / 4.00).toFloat()
        val final = (Math.round(resultado * 10.0) / 10.0).toFloat()
        return final
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {

        val hypotenuse=Math.sqrt((legA*legA)+(legB*legB))

        return hypotenuse
    }
}